// Setup dependencies
const express = require("express");
const mongoose = require("mongoose")

// Create an application using express function
const app = express();
const port = 3001;

const taskRoute = require("./routes/taskRoute")

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/tasks", taskRoute)

app.listen(port, () => console.log(`Server running at port ${port}`));

// Database connection
// Connecting to MongoDB

mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.eiw7ahh.mongodb.net/B243-to-do?retryWrites=true&w=majority",
        {
            useNewUrlParser:true,
            useUnifiedTopology: true
        });