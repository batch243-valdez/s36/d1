// Controllers contain the functions and business logic of our Express JS Application
// Meaning all the operations it can do will be place in this file

    const Task = require("../models/task");

// Controller function for getting all tasks

    module.exports.getAllTasks = () => {
        return Task.find({}).then(result => {
            return result
        })
    }

// Controller function for creating a task

    module.exports.createTask = (requestBody) => {
        
        let newTask = new Task({
            name: requestBody.name
        })  
            return newTask.save().then((task, error) => {
                if(error){
                    console.log(error);
                    return false;
                }
                else{
                    return task;
                }
            })
        }
    
// Control function for deleting a task
    module.exports.deleteTask = (taskId) => {
            return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
                if(err){
                    console.log(err);
                    return false;
                }
                else{
                    return removedTask;
                }
            })
        }

// control function to update a task
    module.exports.updateTask = (taskId, newContent) => {
        return Task.findById(taskId).then((result, error) => {
            if(error){
                console.log(error);
                return false;
            }
            result.name = newContent.name
            return result.save().then((updatedTask, saveErr) => {
                if(saveErr){
                    console.log(saveErr)
                    return false;
                }
                else{
                    return updatedTask
                }
            })
            
        })
    }
          
                









