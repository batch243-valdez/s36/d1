const express = require("express")

// Create a Router instance that functions as middleware and routing system

const router = express.Router();

const taskController = require("../controller/taskController");
const task = require("../models/task");

// [SECTION] ROUTES
// Route to get all the tasks

    router.get("/", (req, res) => {
        taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
    })

// Route to create a new task
// This route expects to receive a POST request at the URL "/tasks"

    router.post("/", (req, res) => {
        taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
    })

// Route to delete a task
// This route expects to receive a DELETE request at the url "/task/:id"

    router.delete("/:id", (req, res) => {
        taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
    })


// Route to Update Task

    router.put("/:id", (req, res) => {
        taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
    })



// Use "Module.exports" to export the router object to use in the app.js
module.exports = router;